# CanModuleVendorLibs

this repo contains all the **original vendor libs and CDs** needed for for CanModule 
https://github.com/quasar-team/CanModule 
and their various versions

Please refer to the validation reports (contact me) for detailed useage, compatibility and performance on the various OSes, vendors and bridge models.

This is for CERN internal use only in order to respect the vendor's copyrights.

The **specific vendor libs** are build by CanModule according to the build chain settings.

https://github.com/quasar-team/CanModule

## windows dependencies
each vendor (anagate, peak, systec) has it's own dll and drivers to be installed. Sometimes they come along as a whole repository/disk/CD/archive containing all sorts of additional tools and architectures. I have just copied the stricly neccessary dll's here.

## Linux cc7, cal9
### USB bridges
(peak, systec) you need to install/run on the host:
- the vendor specific USB driver talking to the specific firmware of the module
- the socketcan USB abstraction driver and lib provided by linux: https://gitlab.cern.ch/mludwig/CAN_libsocketcan

this is usually where problems occur, see:

- (systec) https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=68027711&src=contextnavpagetreemode
- (peak) https://readthedocs.web.cern.ch/display/CANDev/Peak+netdev-socketcan+drivers?src=contextnavpagetreemode

CERN experiments might choose to run their own versions of drivers for safety.

Here are the drivers which work OK:
* systec-atlas (0.9.8p-4.cc7x, udev & RPM): https://gitlab.cern.ch/atlas-dcs-common-software/systec_can
* peak driver (unmodified) https://gitlab.cern.ch/mludwig/peak-usbcan-linux-driver and hereunder
* you also need libsocketcan: git clone https://gitlab.cern.ch/mludwig/CAN_libsocketcan.git


### ethernet bridges 
(anagate) only a vendor specific user space library is needed which wraps the tcp/ip specific
commands into a decent API. For anagate we use what is provided.


see https://readthedocs.web.cern.ch/pages/viewpage.action?pageId=205226150&src=contextnavpagetreemode
for more details



