### changelog for CanModuleVendorLibs
these are the cleaned and cut-down vendor libs needed for CanModule for windows and linux

## 30.april.2024 [master]
- corrected windows/systec "Include" dirs to "include" for more beauty

## 2.march.2023
- structure is 
/opt/3rdPartySoftware/CAN/<OS>/<vendor>/<apiversion>/lib
/opt/3rdPartySoftware/CAN/<OS>/<vendor>/<apiversion>/include
same as in cernbox
for podman: clone everything and delete the OSes which are not needed
we just clone the CAN subdir

## 11 june 2021
- start working on cc8 for systec
- added systec driver snapshot from git

## 25.march.2021
- added peak netdev driver sources for cc7


