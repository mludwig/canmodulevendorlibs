#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/elfnote-lto.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;
BUILD_LTO_INFO;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0x3af7223e, "module_layout" },
	{ 0x16f56967, "param_ops_uint" },
	{ 0x62878f47, "param_ops_bool" },
	{ 0x7f749536, "usb_deregister" },
	{ 0x8c03d20c, "destroy_workqueue" },
	{ 0xd883fead, "usb_register_driver" },
	{ 0xdf9208c0, "alloc_workqueue" },
	{ 0x7a2db36, "sysfs_create_group" },
	{ 0xcefb0c9f, "__mutex_init" },
	{ 0xa1566d53, "unregister_netdev" },
	{ 0xa9d53f4c, "sysfs_remove_group" },
	{ 0xd9a5ea54, "__init_waitqueue_head" },
	{ 0x92997ed8, "_printk" },
	{ 0x23dbabbd, "usb_kill_urb" },
	{ 0x962c8ae1, "usb_kill_anchored_urbs" },
	{ 0x6c693838, "alloc_can_skb" },
	{ 0x72a039be, "can_get_echo_skb" },
	{ 0xc5b6f236, "queue_work_on" },
	{ 0x2d3b6c2c, "netdev_info" },
	{ 0xd950751f, "netif_rx" },
	{ 0x6550c063, "alloc_can_err_skb" },
	{ 0xdc6f3ab1, "consume_skb" },
	{ 0x4439eaf6, "netif_device_detach" },
	{ 0x18d21817, "can_free_echo_skb" },
	{ 0xe0272619, "kfree_skb_reason" },
	{ 0x15ba50a6, "jiffies" },
	{ 0x62b881a0, "can_put_echo_skb" },
	{ 0x3a2b8d6d, "can_dropped_invalid_skb" },
	{ 0x986a212d, "netdev_warn" },
	{ 0x7f430c3f, "usb_free_coherent" },
	{ 0x8b83246f, "usb_unanchor_urb" },
	{ 0xe1f2aec6, "usb_free_urb" },
	{ 0x919626e2, "usb_submit_urb" },
	{ 0x938f0b5f, "usb_anchor_urb" },
	{ 0xf32538f9, "usb_alloc_coherent" },
	{ 0x2bef3831, "usb_alloc_urb" },
	{ 0x3c68f2f9, "netdev_err" },
	{ 0x9f7591c9, "netif_tx_wake_queue" },
	{ 0x7796b0f9, "netdev_printk" },
	{ 0xefc5b546, "devm_kfree" },
	{ 0xc6d09aa9, "release_firmware" },
	{ 0x64773b08, "request_firmware" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0x79d9fc01, "_dev_info" },
	{ 0x97d65876, "_dev_warn" },
	{ 0x37a0cba, "kfree" },
	{ 0x8b4cd20d, "kmem_cache_alloc_trace" },
	{ 0x4ce572de, "kmalloc_caches" },
	{ 0xe05731f4, "devm_kmalloc" },
	{ 0x7cdb8031, "usb_control_msg" },
	{ 0x5c3c7387, "kstrtoull" },
	{ 0x656e4a6e, "snprintf" },
	{ 0x3854774b, "kstrtoll" },
	{ 0x32a096d4, "_dev_err" },
	{ 0x3213f038, "mutex_unlock" },
	{ 0x5d8281a5, "usb_bulk_msg" },
	{ 0x69acdf38, "memcpy" },
	{ 0x4dfa8d4b, "mutex_lock" },
	{ 0xd20fef74, "dev_printk" },
	{ 0xd0da656b, "__stack_chk_fail" },
	{ 0x5b8239ca, "__x86_return_thunk" },
	{ 0xfe916dc6, "hex_dump_to_buffer" },
	{ 0xbdfb6dbb, "__fentry__" },
};

MODULE_INFO(depends, "can-dev");

MODULE_ALIAS("usb:v0878p1103d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1104d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1105d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1121d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1122d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1123d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1125d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1145d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1101d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0878p1181d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "2358C18D2C45AB1C48719DB");
MODULE_INFO(rhelversion, "9.2");
