#!/bin/bash
# run with sudo
# $1 = start port
# $2 = end port
# $3 = bitrate, i.e. 125000 or 250000
for ((p = $1; p <= $2; p++)); do
	echo "ip link set can${p} down"
	ip link set can${p} down
	echo "ip link set can${p} type can bitrate $3"
	ip link set can${p} type can bitrate $3
	echo "ip link set can${p} up"
	ip link set can${p} up
	#echo "ip link show can${p}"
	#ip link show can${p}
done

