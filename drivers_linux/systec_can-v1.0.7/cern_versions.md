=================================================
os & kernel versions for the systec vendor driver
=================================================


cs9
===

5.14.0-176 [5.sept.2023]
------------------------
OK:
[mladmin@pcbe13404 systec_can-v1.0.7]$ uname -a
Linux pcbe13404 5.14.0-176.el9.x86_64 #1 SMP PREEMPT_DYNAMIC Wed Oct 12 08:12:48 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux

[mladmin@pcbe13404 systec_can-v1.0.7]$ make
make -C /lib/modules/5.14.0-176.el9.x86_64/build M=/home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7 modules
make[1]: Entering directory '/usr/src/kernels/5.14.0-176.el9.x86_64'
  CC [M]  /home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7/systec_can.o
  MODPOST /home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7/Module.symvers
  CC [M]  /home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7/systec_can.mod.o
  LD [M]  /home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7/systec_can.ko
  BTF [M] /home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7/systec_can.ko
Skipping BTF generation for /home/mladmin/projects/canmodulevendorlibs/drivers_linux/systec_can-v1.0.7/systec_can.ko due to unavailability of vmlinux
make[1]: Leaving directory '/usr/src/kernels/5.14.0-176.el9.x86_64'



alma9.2
=======

5.14.0-284.25 [5.sept.2023]
---------------------------
FAIL: 
build fails with missing free_candev etc sources/modules

5.14.0-284.18 [5.sept.2023]
---------------------------
FAIL: 
build fails with missing free_candev etc sources/modules


alma9.1
=======

install via PXE/diane almalinux menu PXE 9.1 server
5.14.0-162


