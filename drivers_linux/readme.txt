======
readme
======

cc7 - CERN CentOS7 7.8 64bit gcc 4.6

anagate libs and include files
==============================
libAnagateExt-1.0.3
libAnaGate-1.0.9
anagate-api-2.13
include/

socketcan (for peak and systec: usb as netdev sockets)
======================================================
clone, build and install on your machine:
https://gitlab.cern.ch/mludwig/CAN_libsocketcan.git
build_cc7.sh
make install (installs libsocketcan.a|so in /usr/lib or similar)


systec_can
==========
cloned from 
systec-atlas (0.9.8p-4.cc77, udev & RPM): https://gitlab.cern.ch/atlas-dcs-common-software/systec_can
with a few small scripts added to make it easier

systec-can-v1.0.7 is from SysTec elelectronic GmbH https://www.systec-electronic.com/unternehmen/support/treiber

peak
====
peak driver (unmodified, just a script added) https://gitlab.cern.ch/mludwig/peak-usbcan-linux-driver


cal9

systec_can
==========
use vendor original driver slightly modified for cal9



